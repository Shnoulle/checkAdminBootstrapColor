<?php
/**
 * checkAdminBootstrapColor : Check the color of Bootstrap
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019 Denis Chenu <http://www.sondages.pro>
 * @license WTFPL
 * @version 0.0.0
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * You just DO WHAT THE FUCK YOU WANT TO.
 */
class checkAdminBootstrapColor extends PluginBase
{
    static protected $description = 'See color.';
    static protected $name = 'checkAdminBootstrapColor';

    protected $settings = array(
        'BootstrapColorCheck' => array(
            'type'=>'info',
            'content'=>'To be updated',
        ),
    );

    public function init()
    {
        // No need to register somewhere
    }

    /**
     * @see parent:getPluginSettings
     */
    public function getPluginSettings($getValues=true)
    {
        $content = "";
        $content .= '<h3>Helper classes : Contextual colors</h3>';
        $content .= '<p class="text-muted"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-colors">text-muted (link) </a> : Fusce dapibus, tellus ac cursus commodo, tortor mauris nibh.</p>';
        $content .= '<p class="text-primary"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-colors">text-primary (link) </a> : Nullam id dolor id nibh ultricies vehicula ut id elit.</p>';
        $content .= '<p class="text-success"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-colors">text-success (link) </a> : Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>';
        $content .= '<p class="text-info"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-colors">text-info (link) </a> : Maecenas sed diam eget risus varius blandit sit amet non magna.</p>';
        $content .= '<p class="text-warning"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-colors">text-warning (link) </a> : Etiam porta sem malesuada magna mollis euismod.</p>';
        $content .= '<p class="text-danger"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-colors">text-danger (link) </a> : Donec ullamcorper nulla non metus auctor fringilla.</p>';
        $content .= '<h3>Helper classes : Contextual backgrounds</h3>';
        $content .= '<p class="bg-primary"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-backgrounds">bg-primary (link)</a>: Nullam id dolor id nibh ultricies vehicula ut id elit.</p>';
        $content .= '<p class="bg-success"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-backgrounds">bg-success (link)</a> : Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>';
        $content .= '<p class="bg-info"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-backgrounds">bg-info (link)</a> : Maecenas sed diam eget risus varius blandit sit amet non magna.</p>';
        $content .= '<p class="bg-warning"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-backgrounds">bg-warning (link)</a> : Etiam porta sem malesuada magna mollis euismod.</p>';
        $content .= '<p class="bg-danger"><a href="https://getbootstrap.com/docs/3.3/css/#helper-classes-backgrounds">bg-danger (link)</a> : Donec ullamcorper nulla non metus auctor fringilla.</p>';
        $content .= '<h3>Labels variations</h3>';
        $content .= ' <span class="label label-default">Default</span> <span class="label label-primary">Primary</span> <span class="label label-success">Success</span> <span class="label label-info">Info</span> <span class="label label-warning">Warning</span> <span class="label label-danger">Danger</span> ';
        $content .= '<h3>Alerts</h3>';
        $content .= '<div class="alert alert-success" role="alert"> <strong>Well done!</strong> You successfully read this important alert message. <a href="https://getbootstrap.com/docs/3.3/components/#alerts" class="alert-link">With a link.</a></div>';
        $content .= '<div class="alert alert-info" role="alert"> <strong>Heads up!</strong> This alert needs your attention, but it\'s not super important. <a href="https://getbootstrap.com/docs/3.3/components/#alerts" class="alert-link">With a link.</a></div>';
        $content .= '<div class="alert alert-warning" role="alert"> <strong>Warning!</strong> Better check yourself, you\'re not looking too good. <a href="https://getbootstrap.com/docs/3.3/components/#alerts" class="alert-link">With a link.</a></div>';
        $content .= '<div class="alert alert-danger" role="alert"> <strong>Oh snap!</strong> Change a few things up and try submitting again. <a href="https://getbootstrap.com/docs/3.3/components/#alerts" class="alert-link">With a link.</a></div>';
        $content .= '<h3>Panels</h3>';
        $content .= '<div class="panel panel-default"> <div class="panel-heading"> <h3 class="panel-title">Panel default</h3> </div> <div class="panel-body"> Panel content </div> </div>';
        $content .= '<div class="panel panel-primary"> <div class="panel-heading"> <h3 class="panel-title">Panel primary</h3> </div> <div class="panel-body"> Panel content </div> </div>';
        $content .= '<div class="panel panel-success"> <div class="panel-heading"> <h3 class="panel-title">Panel success</h3> </div> <div class="panel-body"> Panel content </div> </div>';
        $content .= '<div class="panel panel-info"> <div class="panel-heading"> <h3 class="panel-title">Panel info</h3> </div> <div class="panel-body"> Panel content </div> </div>';
        $content .= '<div class="panel panel-warning"> <div class="panel-heading"> <h3 class="panel-title">Panel warning</h3> </div> <div class="panel-body"> Panel content </div> </div>';
        $content .= '<div class="panel panel-danger"> <div class="panel-heading"> <h3 class="panel-title">Panel danger</h3> </div> <div class="panel-body"> Panel content </div> </div>';
        $content .= '<h3>Buttons</h3>';
        $content .= '<button type="button" class="btn btn-default">Default</button>';
        $content .= '<button type="button" class="btn btn-primary">Primary</button>';
        $content .= '<button type="button" class="btn btn-success">Success</button>';
        $content .= '<button type="button" class="btn btn-info">Info</button>';
        $content .= '<button type="button" class="btn btn-warning">Warning</button>';
        $content .= '<button type="button" class="btn btn-danger">Danger</button>';
        $content .= '<button type="button" class="btn btn-link">Link</button>';
        $content .= '';

        $content .= '<h3>Table Contextual classes</h3>';
        $content .= '<table class="table"> <thead> <tr> <th>#</th> <th>Column heading</th> <th>Column heading</th> <th>Column heading</th> </tr> </thead> <tbody> <tr class="active"> <th scope="row">1</th> <td>Active row</td> <td>Column content</td> <td>Column content</td> </tr> <tr> <th scope="row">2</th> <td>Column content</td> <td>Column content</td> <td>Column content</td> </tr> <tr class="success"> <th scope="row">3</th> <td>success row</td> <td>Column content</td> <td>Column content</td> </tr> <tr> <th scope="row">4</th> <td>Column content</td> <td>Column content</td> <td>Column content</td> </tr> <tr class="info"> <th scope="row">5</th> <td>Info row</td> <td>Column content</td> <td>Column content</td> </tr> <tr> <th scope="row">6</th> <td>Column content</td> <td>Column content</td> <td>Column content</td> </tr> <tr class="warning"> <th scope="row">7</th> <td>Warning row</td> <td>Column content</td> <td>Column content</td> </tr> <tr> <th scope="row">8</th> <td>Column content</td> <td>Column content</td> <td>Column content</td> </tr> <tr class="danger"> <th scope="row">9</th> <td>Danger row</td> <td>Column content</td> <td>Column content</td> </tr> </tbody> </table>';
        

        $this->settings['BootstrapColorCheck']['content'] = $content;
        return parent::getPluginSettings($getValues);
    }
}
